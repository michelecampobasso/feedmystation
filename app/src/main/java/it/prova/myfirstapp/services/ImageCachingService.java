package it.prova.myfirstapp.services;

import android.content.Context;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import it.prova.myfirstapp.MainActivity;
import it.prova.myfirstapp.R;

public class ImageCachingService {

    public static File getCacheFolder(Context context) {
        File cacheDir = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            cacheDir = new File(Environment.getExternalStorageDirectory(), "cache");
            if(!cacheDir.isDirectory()) {
                cacheDir.mkdirs();
            }
        }

        if(!cacheDir.isDirectory()) {
            cacheDir = context.getCacheDir(); //get system cache folder
        }

        return cacheDir;
    }

    public static void writeToCacheFolder(String wallpaperURLStr, Context context) {
        try {
            URL wallpaperURL;
            if (wallpaperURLStr == null || wallpaperURLStr == "")
                return;
            if (wallpaperURLStr.startsWith("http://"))
                wallpaperURL = new URL(wallpaperURLStr);
            else
                wallpaperURL = new URL("http://"+wallpaperURLStr);
            InputStream inputStream = new BufferedInputStream(wallpaperURL.openStream(), 10240);
            File cacheDir = getCacheFolder(context);
            String fileName = wallpaperURLStr.substring(wallpaperURLStr.lastIndexOf("/") + 1);
            File cacheFile = new File(cacheDir, fileName);

            FileOutputStream outputStream = null;

            outputStream = new FileOutputStream(cacheFile);
            byte buffer[] = new byte[1024];
            int dataSize;
            int loadedSize = 0;
            while ((dataSize = inputStream.read(buffer)) != -1) {
                loadedSize += dataSize;
                outputStream.write(buffer, 0, dataSize);
            }

            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public static Bitmap loadFromCacheFolder(String imageName, Context context) {
        try {
            File cacheDir = getCacheFolder(context);
            File cacheFile = new File(cacheDir, imageName.substring(imageName.lastIndexOf("/") + 1));
            InputStream fileInputStream = null;
            fileInputStream = new FileInputStream(cacheFile);
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(fileInputStream, null, bitmapOptions);
        } catch (FileNotFoundException | NullPointerException e) {
            return null;
        }

    }
}
