package it.prova.myfirstapp.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateHelper {

    private static DateFormat US = new SimpleDateFormat("EEE, dd MMM yyy HH:mm:ss Z", Locale.US);
    private static DateFormat US_noZone = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static DateFormat GMT = new SimpleDateFormat("EEE, dd MMM yyy HH:mm:ss Z", Locale.US);


    public static String relativeDateToString(String date, String df) {
        if (df == "US")
            return relativeDateToStringUS(date);
        if (df == "US_noZone")
            return relativeDateToStringUS_noZone(date);
        return null; //should never be hit...
    }

    public static String relativeDateToStringUS(String date) {
        String pubDate = "";
        try {
            //Creo una data per il feed...
            Calendar feedDate = Calendar.getInstance();
            if (date.contains("GMT"))
                feedDate.setTime(GMT.parse(date));
            else
                feedDate.setTime(US.parse(date));
            //...da paragonare a quella odierna
            Calendar now = Calendar.getInstance();
            if (now.get(Calendar.DAY_OF_MONTH) == feedDate.get(Calendar.DAY_OF_MONTH))
                pubDate = "Oggi, ";
            else {
                //HO BISOGNO DI UNA VARIABILE DI APPOGGIO...
                Calendar dayPlusOne = feedDate;
                dayPlusOne.add(Calendar.DAY_OF_MONTH, 1);
                if (now.get(Calendar.DAY_OF_MONTH) == dayPlusOne.get(Calendar.DAY_OF_MONTH))
                    pubDate = "Ieri, ";
                else {
                    int day = feedDate.get(Calendar.DAY_OF_MONTH)-1; //E POI LE DATE VANNO DA DUE?!?!
                    int month = feedDate.get(Calendar.MONTH) + 1; //JAVA FA CACARE
                    pubDate = ((day < 10) ? "0" + day : day) + "-" + ((month < 10) ? "0" + month : month) + ", ";
                }
            }
            int hours = feedDate.get(Calendar.HOUR_OF_DAY);
            int minutes = feedDate.get(Calendar.MINUTE);
            pubDate += ((hours < 10) ? "0"+hours : hours) +":"+((minutes < 10) ? "0"+minutes : minutes);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return pubDate;
    }

    public static String relativeDateToStringUS_noZone(String date) {
        String pubDate = "";
        try {
            Calendar forecastDate = Calendar.getInstance();
            forecastDate.setTime(US_noZone.parse(date));
            Calendar now = Calendar.getInstance();
            if (now.get(Calendar.DAY_OF_MONTH) == forecastDate.get(Calendar.DAY_OF_MONTH))
                pubDate = "Oggi, ";
            else
                pubDate = "Domani, ";
            int hours = forecastDate.get(Calendar.HOUR_OF_DAY);
            int minutes = forecastDate.get(Calendar.MINUTE);
            pubDate += ((hours < 10) ? "0"+hours : hours) +":"+((minutes < 10) ? "0"+minutes : minutes);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return pubDate;
    }
}
