package it.prova.myfirstapp.services;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;

import it.prova.myfirstapp.model.Forecast;


public class ForecastsService {


    public ArrayList<ArrayList<Forecast>> getForecasts(String url1, String url2) {

        ArrayList<ArrayList<Forecast>> results = new ArrayList<>();
        results.add(getCityForecasts(url1));
        results.add(getCityForecasts(url2));
        return results;
    }

    private ArrayList<Forecast> getCityForecasts(String url) {
        HttpHandler sh = new HttpHandler();

        // Making a request to url and getting response
        String jsonStr = sh.makeServiceCall(url);
        ArrayList<Forecast> results = new ArrayList<>();
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                // Gathering city informations
                JSONObject city = jsonObj.getJSONObject("city");
                String cityName = city.getString("name");
                // Getting JSON Array node
                JSONArray forecasts = jsonObj.getJSONArray("list");

                // looping through all forecasts
                for (int i = 0; i < 8; i++) {
                    JSONObject c = forecasts.getJSONObject(i);

                    JSONObject main = c.getJSONObject("main");
                    DecimalFormat df = new DecimalFormat("##.##");
                    String temp = String.valueOf(df.format(main.getDouble("temp")-273.15))+"ºC";

                    JSONArray weatherArr = c.getJSONArray("weather");
                    JSONObject weather = weatherArr.getJSONObject(0);
                    String iconName = weather.getString("icon");
                    //Correzione delle previsioni ridicole di pioggia leggera
                    if (iconName.equalsIgnoreCase("10d") || iconName.equalsIgnoreCase("10n")) {
                        if ((c.getJSONObject("rain").has("3h")))
                            //Se la pioggia è inferiore al millimetro
                            if ((c.getJSONObject("rain").getDouble("3h"))<1)
                                //E se è nuvoloso... (Ste previsioni fanno merda)
                                if ((c.getJSONObject("clouds").has("all") && c.getJSONObject("clouds").getInt("all")>50))
                                    iconName="03d";
                                //Se lo è poco...
                                else
                                    //...ed è notte
                                    if (iconName.contains("n"))
                                        iconName="02n";
                                    //...ed è giorno
                                    else
                                        iconName="02d";
                    }

                    JSONObject wind = c.getJSONObject("wind");
                    String windSpeed = String.valueOf(df.format(wind.getDouble("speed")/0.36))+" km/h"; //I venti sono espressi in m/s, ma sembra essere un decimo del loro valore

                    String date = DateHelper.relativeDateToString(c.getString("dt_txt"), "US_noZone");

                    results.add(new Forecast(iconName, temp, windSpeed, date, cityName));
                }
            } catch (final JSONException e) {
                return new ArrayList<>();
            }
        }
        else
            return new ArrayList<>();
        return results;
    }


    private class HttpHandler {

        private final String TAG = HttpHandler.class.getSimpleName();

        public HttpHandler() {
        }

        public String makeServiceCall(String reqUrl) {
            String response = null;
            try {
                URL url = new URL(reqUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                // read the response
                InputStream in = new BufferedInputStream(conn.getInputStream());
                response = convertStreamToString(in);
            } catch (MalformedURLException e) {
                Log.e(TAG, "MalformedURLException: " + e.getMessage());
            } catch (ProtocolException e) {
                Log.e(TAG, "ProtocolException: " + e.getMessage());
            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
            return response;
        }

        private String convertStreamToString(InputStream is) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append('\n');
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }
    }
}


