package it.prova.myfirstapp.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.prova.myfirstapp.model.feeds.Feed;
import it.prova.myfirstapp.model.Rating;

public class RatingService {

    private ArrayList<Rating> ratings = new ArrayList<>();

    public RatingService(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        Map settingsMap = settings.getAll();
        Set<String> keySet = settingsMap.keySet();

        List<String> sortedKeySet = SortUtils.asSortedList(keySet);
        for (int i = 0; i<sortedKeySet.size(); i+=2) {
            // Questo si romperà nel momento in cui arriveranno impostazioni con un nome che comincia per "key" o minore
            if (sortedKeySet.get(i).contains("keywordScore")) {
                int currentScore = Integer.parseInt(settingsMap.get(sortedKeySet.get(i)).toString());
                String currentKeyword = settingsMap.get(sortedKeySet.get(i + 1)).toString();
                ratings.add(new Rating (currentKeyword, currentScore));
            }

        }
    }

    public int CalculateRating(String text, int score) {
        for (Rating r : ratings)
            if (text.contains(r.getKeyword()))
                score += r.getScore();
        return score;
    }
}
