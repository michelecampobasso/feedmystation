package it.prova.myfirstapp.engine;

import android.content.Context;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import it.prova.myfirstapp.R;
import it.prova.myfirstapp.model.feeds.GoogleFeed;
import it.prova.myfirstapp.services.RatingService;

public class GoogleFeedEngine {

    private XmlPullParserFactory xmlFactoryObject;
    private XmlPullParser myParser;
    HttpURLConnection urlConnection;
    private ArrayList<GoogleFeed> retval = new ArrayList<>();

    public ArrayList<GoogleFeed> getFeeds(Context context) {
        try {
            //Questo URL va costruito, a differenza degli altri, in quanto permette query al suo interno
            URL url = new URL(context.getString(R.string.google_feed));
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            xmlFactoryObject = XmlPullParserFactory.newInstance();
            xmlFactoryObject.setNamespaceAware(true);
            myParser = xmlFactoryObject.newPullParser();

            myParser.setInput(in, null);

            RatingService ratingService = new RatingService(context);

            String text = "";
            GoogleFeed result = new GoogleFeed();
            int eventType = myParser.getEventType();
            String oldTagname = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = myParser.getName();
                switch (eventType) {
                    case XmlPullParser.TEXT:
                        text = myParser.getText();

                        //Rating here!
                        if (oldTagname!=null && (oldTagname.equalsIgnoreCase("title") || oldTagname.equalsIgnoreCase("description")))
                        result.setRating(ratingService.CalculateRating(text, result.getRating()));

                        break;
                    case XmlPullParser.END_TAG:
                        if (!(text.equals("") && text.equals(null))) {
                            if (tagname.equalsIgnoreCase("title"))
                                result.setTitle(text);
                            if (tagname.equalsIgnoreCase("pubdate"))
                                result.setPubDate(text);
                            if (tagname.equals("description")) {
                                //Trattamento speciale per te, merdaccia
                                //Parto da inizio di img src, poi c'è ="// quindi aggiungo 4
                                //Ottengo la tringa che comincia con l'url e contiene il resto
                                if (text.contains("img src")) {
                                    String imgurl = text.substring(text.indexOf("img src") + 11, text.length());
                                    result.setImageUrl(imgurl.substring(0, imgurl.indexOf("\"")));
                                } else
                                    result.setImageUrl("");
                            }
                            if (tagname.equalsIgnoreCase("link"))
                                result.setUrl(text);
                            if (tagname.equalsIgnoreCase("item")) {
                                retval.add(result);
                                result = new GoogleFeed();
                            }
                        }
                        break;

                    default:
                        break;
                }
                oldTagname = tagname;
                eventType = myParser.next();
            }

        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

        return retval;
    }
}
