package it.prova.myfirstapp.engine.controller;

import android.content.Context;

import java.util.ArrayList;

import it.prova.myfirstapp.R;
import it.prova.myfirstapp.model.Forecast;
import it.prova.myfirstapp.services.ForecastsService;

public class ForecastsController {

    public ArrayList<ArrayList<Forecast>> getForecasts(Context context) {
        return new ForecastsService().getForecasts(context.getString(R.string.forecasts_api_bo), context.getString(R.string.forecasts_api_fc));
    }
}
