package it.prova.myfirstapp.engine.controller;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import it.prova.myfirstapp.engine.CorriereFeedEngine;
import it.prova.myfirstapp.engine.GoogleFeedEngine;
import it.prova.myfirstapp.engine.RepubblicaFeedEngine;
import it.prova.myfirstapp.model.feeds.Feed;

public class FeedEngineController {

    public ArrayList<Feed> getFeeds(Context context) {
        ArrayList<Feed> feeds = new ArrayList<>();
        feeds.addAll(new RepubblicaFeedEngine().getFeeds(context));
        feeds.addAll(new CorriereFeedEngine().getFeeds(context));
        feeds.addAll(new GoogleFeedEngine().getFeeds(context));
        sortByRating(feeds);
        return feeds;
    }

    public void sortByRating(ArrayList<Feed> news) {
        Collections.sort(news, new FeedComparator());
    }

    public class FeedComparator implements Comparator<Feed>{

        @Override
        public int compare(Feed lhs, Feed rhs) {
            return rhs.getRating()-lhs.getRating();
        }
    }
}
