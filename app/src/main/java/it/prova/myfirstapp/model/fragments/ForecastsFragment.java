package it.prova.myfirstapp.model.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import it.prova.myfirstapp.R;
import it.prova.myfirstapp.engine.controller.ForecastsController;
import it.prova.myfirstapp.model.Forecast;
import it.prova.myfirstapp.model.adapters.ForecastsAdapter;

public class ForecastsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView lv = (ListView) inflater.inflate(R.layout.forecasts_fragment, container, false);

        ArrayList<ArrayList<Forecast>> forecasts = new ForecastsController().getForecasts(getContext());
        ForecastsAdapter arrayAdapter = new ForecastsAdapter(getContext(), R.layout.forecasts_rows, forecasts);
        lv.setAdapter(arrayAdapter);

        return lv;
    }
}