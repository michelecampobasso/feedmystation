package it.prova.myfirstapp.model.preferences;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import it.prova.myfirstapp.R;


public class MyCustomDialogPreference extends DialogPreference {

    private EditText keywordEditText;
    private EditText scoreEditText;

    public MyCustomDialogPreference (Context context, AttributeSet attrs) {
        super(context, attrs);
        setPersistent(false);
        setDialogLayoutResource(R.layout.keyword_dialog);
    }

    @Override
    public void onBindDialogView(View view) {
        super.onBindDialogView(view);
        keywordEditText = (EditText) view.findViewById(R.id.keywordText);
        scoreEditText = (EditText) view.findViewById(R.id.keywordScore);

        //TextWatcher checking the score
        TextWatcher scoreChecker = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if ((s != null) && !s.toString().equals("")) {
                    int assignedScore = Integer.parseInt(s.toString());
                    if (assignedScore < 1 || assignedScore > 5)
                        s.replace(0, s.length(), "", 0, 0);
                }
            }
        };
        //Attaching the score checker
        scoreEditText.addTextChangedListener(scoreChecker);

        //Loading installed preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        try {
            String keyword = preferences.getAll().get(this.getKey()+"keywordText").toString();
            if (keyword != null)
                keywordEditText.setText(keyword);
            String score = preferences.getAll().get(this.getKey()+"keywordScore").toString();
            if (score != null)
                scoreEditText.setText(score);
        }
        catch (NullPointerException e)
        {
            System.out.println("Not yet initialized.");
        }
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            updatePref(this.getKey()+"keywordText", keywordEditText.getText().toString());
            updatePref(this.getKey()+"keywordScore", scoreEditText.getText().toString());
        }
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        builder.setTitle(R.string.keyword_dialog_title);
        builder.setMessage(R.string.keyword_dialog_summary);
        super.onPrepareDialogBuilder(builder);
    }

    private void updatePref(String pref, String newValue){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(pref, newValue);
        editor.apply();
    }
}
