package it.prova.myfirstapp.model;

public class Forecast {

    private String iconName;
    private String temp;
    private String wind;
    private String date;
    private String city;

    public Forecast(String in, String t, String w, String d, String ci) {
        iconName = in;
        temp = t;
        wind = w;
        date = d;
        city = ci;
    }

    public String getTemp() { return temp; }

    public void setTemp(String temp1) { temp = temp1; }

    public String getWind() { return wind; }

    public void setWind(String wind1) { wind = wind1; }

    public String getIconName() { return iconName; }

    public void setIconName(String iconName1) { iconName = iconName1; }

    public String getDate() { return date; }

    public void setDate(String date1) { date = date1; }

    public String getCity() { return city; }

    public void setCity(String city1) { city = city1; }

}
