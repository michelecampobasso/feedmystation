package it.prova.myfirstapp.model.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import it.prova.myfirstapp.R;
import it.prova.myfirstapp.engine.controller.FeedEngineController;
import it.prova.myfirstapp.model.feeds.Feed;
import it.prova.myfirstapp.model.adapters.FeedAdapter;

public class FeedFragment extends Fragment {

    private ArrayList<Feed> feeds = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Creo il contenuto del frammento Feed
        ListView lv = (ListView) inflater.inflate(R.layout.feed_fragment, container, false);

        //Chiamo il controller del motore che recupera i feed
        feeds = new FeedEngineController().getFeeds(getContext());

        FeedAdapter arrayAdapter = new FeedAdapter(getContext(), R.layout.list_row, feeds);
        lv.setAdapter(arrayAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openNews(view, position);
            }
        });

        return lv;

    }

    public void openNews(View view, int position) {
        Uri uriUrl = Uri.parse(feeds.get(position).getUrl());
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}