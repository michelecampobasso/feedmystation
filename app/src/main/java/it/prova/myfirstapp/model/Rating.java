package it.prova.myfirstapp.model;

public class Rating {

    private int score;
    private String keyword;

    public Rating(String k, int s) {
        keyword = k;
        score = s;
    }

    public void setScore(int s) { score = s; }

    public int getScore() { return score; }

    public void setKeyword(String k) { keyword = k; }

    public String getKeyword() { return keyword; }

}
