package it.prova.myfirstapp.model.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import it.prova.myfirstapp.R;
import it.prova.myfirstapp.model.feeds.Feed;
import it.prova.myfirstapp.services.ImageCachingService;

public class FeedAdapter extends ArrayAdapter<Feed> {
    private final Context context;
    private final ArrayList<Feed> data;
    private final int layoutResourceId;

    public FeedAdapter(Context context, int layoutResourceId, ArrayList<Feed> data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
            holder.title = (TextView)row.findViewById(R.id.title);
            holder.score = (TextView)row.findViewById(R.id.score);
            holder.date = (TextView)row.findViewById(R.id.date);

            row.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)row.getTag();
        }

        Feed feed = data.get(position);

        holder.title.setText(feed.getTitle());
        holder.score.setText(String.valueOf(feed.getRating()));
        holder.date.setText(feed.getPubDate());

        holder.list_image = (ImageView)row.findViewById(R.id.list_image);
        holder.list_image.setImageDrawable(null);
        row.setTag(holder);

        holder = (ViewHolder)row.getTag();
        holder.thumbnailUrl = feed.getImageUrl();
        if (holder.thumbnailUrl != null &&  holder.thumbnailUrl != "") {
            if ((holder.bitmap = ImageCachingService.loadFromCacheFolder(holder.thumbnailUrl, context)) == null)
                new DownloadAsyncTask().execute(holder);
            holder.list_image.setImageBitmap(holder.bitmap);
        }
        else {
            holder.list_image.setImageResource(R.drawable.noimage);
        }
        return row;
    }

    static class ViewHolder
    {
        TextView title;
        TextView score;
        TextView date;
        ImageView list_image;
        String thumbnailUrl;
        Bitmap bitmap;
    }

    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {

        @Override
        protected ViewHolder doInBackground(ViewHolder... params) {
            // TODO Auto-generated method stub
            //load image directly
            ViewHolder viewHolder = params[0];
            if (viewHolder.thumbnailUrl == null || viewHolder.thumbnailUrl == "")
                return viewHolder;
            if ((viewHolder.bitmap = ImageCachingService.loadFromCacheFolder(viewHolder.thumbnailUrl, context)) == null) {
                //Se assente in cache
                ImageCachingService.writeToCacheFolder(viewHolder.thumbnailUrl, context);
                viewHolder.bitmap = ImageCachingService.loadFromCacheFolder(viewHolder.thumbnailUrl, context);
            }
            return viewHolder;
        }

        @Override
        protected void onPostExecute(ViewHolder result) {
            // TODO Auto-generated method stub
            if (result.bitmap == null || result.thumbnailUrl == null || result.thumbnailUrl == "") {
                result.list_image.setImageResource(R.drawable.noimage);
            } else {
                result.list_image.setImageBitmap(result.bitmap);
            }
        }
    }
}