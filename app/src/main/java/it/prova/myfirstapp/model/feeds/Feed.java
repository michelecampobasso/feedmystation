package it.prova.myfirstapp.model.feeds;

import java.util.ArrayList;

import it.prova.myfirstapp.services.DateHelper;

public abstract class Feed {

    private String title;
    private String url;
    private int rating = 0;
    private String imageUrl;
    private String pubDate;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }

    public int getRating() { return rating; }

    public void setRating(int rating) { this.rating = rating; }

    public String getImageUrl() { return imageUrl; }

    public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }

    public String getPubDate() { return pubDate; }

    public void setPubDate(String date) {
        pubDate = DateHelper.relativeDateToString(date, "US");
    }
}
