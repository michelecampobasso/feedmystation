package it.prova.myfirstapp.model.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import junit.framework.Assert;

import java.util.ArrayList;

import it.prova.myfirstapp.R;
import it.prova.myfirstapp.model.Forecast;

public class ForecastsAdapter extends ArrayAdapter<ArrayList<Forecast>> {
    private final Context context;
    private final ArrayList<ArrayList<Forecast>> data;
    private final ArrayList<Forecast> data1;
    private final ArrayList<Forecast> data2;
    private final int layoutResourceId;

    public ForecastsAdapter(Context context, int layoutResourceId, ArrayList<ArrayList<Forecast>> data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.data1 = data.get(0);
        this.data2 = data.get(1);
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public int getCount(){
        return data!=null ? data.get(0).size() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
            holder.first = row.findViewById(R.id.city1);
            holder.second = row.findViewById(R.id.city2);
            holder.hour = (TextView)holder.first.findViewById(R.id.forecast_date);
            holder.temp = (TextView)holder.first.findViewById(R.id.forecast_temp);
            holder.wind = (TextView)holder.first.findViewById(R.id.forecast_wind);

            holder.hour2 = (TextView)holder.second.findViewById(R.id.forecast_date);
            holder.temp2 = (TextView)holder.second.findViewById(R.id.forecast_temp);
            holder.wind2 = (TextView)holder.second.findViewById(R.id.forecast_wind);

            row.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)row.getTag();
        }

        Forecast f1 = data1.get(position);
        Forecast f2 = data2.get(position);

        holder.hour.setText(f1.getDate());
        holder.temp.setText(f1.getTemp());
        holder.thumb = (ImageView)holder.first.findViewById(R.id.list_image);
        holder.thumb.setImageDrawable(null);
        holder.thumb.setImageResource(getDrawable(context, f1.getIconName()));
        holder.wind.setText(f1.getWind());

        holder.hour2.setText(f2.getDate());
        holder.temp2.setText(f2.getTemp());
        holder.thumb2 = (ImageView)holder.second.findViewById(R.id.list_image);
        holder.thumb2.setImageDrawable(null);
        holder.thumb2.setImageResource(getDrawable(context, f2.getIconName()));
        holder.wind2.setText(f2.getWind());

        return row;
    }

    public static int getDrawable(Context context, String name)
    {
        Assert.assertNotNull(context);
        Assert.assertNotNull(name);
        int retval = context.getResources().getIdentifier("i" + name, "drawable", context.getPackageName());
        return retval;
    }

    @Override
    public boolean isEnabled (int position) {
        return false;
    }

    final class ViewHolder
    {
        View first;
        View second;
        TextView temp;
        TextView wind;
        TextView hour;
        ImageView thumb;
        TextView temp2;
        TextView wind2;
        TextView hour2;
        ImageView thumb2;
    }
}
